package com.openweb.employee.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.openweb.employee.domain.Employee;
import com.openweb.employee.exception.EmployeeRecordNotFoundException;
import com.openweb.employee.service.EmployeeService;

@Controller
public class EmployeeController {

	private EmployeeService employeeService;

	@Autowired
	public EmployeeController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@RequestMapping("/employee")
	@ResponseBody
	public List<Employee> getEmployees(
			@RequestParam(value = "skillset", required = false, defaultValue = "All") Set<String> skillset) {
		if (skillset.contains("All"))
			return employeeService.getAll();
		else {
			return employeeService.getAll().stream()
					.filter(employee -> CollectionUtils.containsAny(employee.getSkills(), skillset))
					.collect(Collectors.toList());
		}
	}

	
	@RequestMapping("/employee/new")
	@ResponseBody
	public Employee createNewEmployee(@RequestBody Employee employee) {
		return employeeService.create(employee);

	}

	@RequestMapping("/employee/update")
	@ResponseBody
	public Employee updateEmployee(@RequestBody Employee employee) throws EmployeeRecordNotFoundException {
		return employeeService.update(employee);

	}

	@RequestMapping("/employee/delete")
	@ResponseBody
	public void deleteEmployee(@RequestBody Employee employee) throws EmployeeRecordNotFoundException {
		employeeService.delete(employee.getId());

	}

}
