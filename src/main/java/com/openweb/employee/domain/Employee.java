package com.openweb.employee.domain;

import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document
public class Employee {

	@Id
	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("role")
	private String role;
	@JsonProperty("bio")
	private String bio;
	@JsonProperty("skills")
	private Set<String> skills;
	@JsonProperty("profileImage")
	private String profileImage;

	public Employee(String id, String name, String role, String bio, Set<String> skills, String profileImage) {
		
		this.id = id;
		this.name = name;
		this.role = role;
		this.bio = bio;
		this.skills = skills;
		this.profileImage = profileImage;
	}
	
	public Employee(){
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Set<String> getSkills() {
		return skills;
	}

	public void setSkills(Set<String> skills) {
		this.skills = skills;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

}
