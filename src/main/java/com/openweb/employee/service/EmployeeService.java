package com.openweb.employee.service;

import java.util.List;
import com.openweb.employee.domain.Employee;
import com.openweb.employee.exception.EmployeeRecordNotFoundException;

public interface EmployeeService {

	List<Employee> getAll();

	boolean employeeExists(String id);

	Employee create(Employee employee);

	Employee update(Employee employee) throws EmployeeRecordNotFoundException;

	void delete(String id) throws EmployeeRecordNotFoundException;

}
