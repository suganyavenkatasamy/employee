package com.openweb.employee.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openweb.employee.domain.Employee;
import com.openweb.employee.exception.EmployeeRecordNotFoundException;
import com.openweb.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	@Override
	public List<Employee> getAll() {
		List<Employee> employeeList = new ArrayList<>();
		employeeRepository.findAll().forEach(employeeList::add);
		return employeeList;
	}

	@Override
	public boolean employeeExists(String empId) {
		return employeeRepository.exists(empId);
	}

	@Override
	public Employee create(Employee employee) {
		employeeRepository.save(employee);
		return employee;
	}

	@Override
	public Employee update(Employee employee) throws EmployeeRecordNotFoundException {
		if (employeeRepository.exists(employee.getId())) {
			return employeeRepository.save(employee);
		} else {
			throw new EmployeeRecordNotFoundException();
		}

	}

	@Override
	public void delete(String empId) throws EmployeeRecordNotFoundException {
		if (employeeRepository.exists(empId)) {
			employeeRepository.delete(empId);
		} else {
			throw new EmployeeRecordNotFoundException();
		}

	}

}
