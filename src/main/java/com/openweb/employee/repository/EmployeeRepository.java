package com.openweb.employee.repository;

import org.springframework.data.repository.CrudRepository;
import com.openweb.employee.domain.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, String> {

}
