package com.openweb.employee.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openweb.employee.domain.Employee;
import com.openweb.employee.repository.EmployeeRepository;
import com.openweb.employee.service.EmployeeService;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

	@Autowired
	MockMvc mockMvc;
	@MockBean
	private EmployeeService empService;
	@MockBean
	private EmployeeRepository empRepo;

	@Test
	public void testCreateEmployeesSuccessfully() throws Exception {
		Employee emp = new Employee("456", "suganya", "DevOps Engineer", "", new HashSet<String>(Arrays.asList("java")),
				"Lakshana.jpg");

		when(empRepo.save(emp)).thenReturn(emp);

		mockMvc.perform(put("/employee/new").contentType(MediaType.APPLICATION_JSON).content(asJsonString(emp)))

				.andExpect(status().isOk());

	}

	/*
	 * TODO for Update,get and delete
	 */
	public static String asJsonString(final Employee obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
