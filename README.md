# README #

The code exposes Rest endpoints for the employee Records. The employee records are stored in MongoDB. 
It support 4 operations. The create,update and delete operation requries json as input

Create -   http://localhost:8080/employee/new
   
Update -http://localhost:8080/employee/new (Throws EmployeeRREcordNotFoundException if the id doesnt exists in the mongoDB

Delete - http://localhost:8080/employee/delete (Throws EmployeeRREcordNotFoundException if the id doesnt exists in the mongoDB
   
List all employees - http://localhost:8080/employee - Returns json Array containing Employee List.

http://localhost:8080/employee?skillset=java - Returns employees filtered based on skill ser specified.

Pre Setup:
Install and run mongoDB prior to start the spring boot mvn spring-boor:run


JsonInput:
  {
  "id":"1234",
  "name": "Omar Rass",
  "role": "Scrum Master",
  "bio": "My name is Omar and I�m an Agile Project Manager and Scrum Master that lives and breathes Agile. In addition, I�m experienced in E-commerce and like to be as much all-round as possible. I enjoy helping teams to achieve their goals and have fun doing so. In the past, I worked with the founders of Open Web and I am enjoying working with them again. When I�m not working, I enjoy spending quality time with my family and friends.",
  "skills": ["scrum"],
  "profileImage": "https://en.openweb.nl/binaries/teaser/content/gallery/ow/employees/omar.jpg"
}